# Telepaton

## Requirements

* Linux Ubuntu 18.04
* Docker 19.03

Inicialize o cluster Swarm:

```shell
docker swarm init
```

```shell
sysctl -w vm.max_map_count=262144
```

## Build

```shell
git clone git@gitlab.com:telepaton/api.git
docker build -t telepaton-api api
```

```shell
git clone git@gitlab.com:telepaton/chat.git
docker build -t telepaton-app chat
```

## Deploy

```shell
git clone git@gitlab.com:telepaton/stack.git
docker stack deploy --compose-file stack/docker-stack.yml telepaton
```
